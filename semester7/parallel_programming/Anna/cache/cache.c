#include <stdio.h>
#include <inttypes.h>
#include <malloc.h>

#ifdef __i386
static __inline__ uint64_t rdtsc() {
	uint64_t x;
	__asm__ volatile ("rdtsc" : "=A" (x));
	return x;
}
#elif __amd64
static __inline__ uint64_t rdtsc() {
	uint64_t a, d;
	__asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
	return (d << 32) | a;
}
#endif

unsigned int Blocksize = 262144; //64 * 4096
unsigned int Offset = 4194304; //1024 * 4096

int main() {

	uint64_t rdtscStart;
	unsigned int *mass, i, j, k, n;

	FILE *fp1;
	FILE *fp2;
	fp1 = fopen("out1.txt", "w");
	fp2 = fopen("out2.txt", "w");
	for(n = 1; n <= 32; n++) {
		mass = (int*)malloc(Offset * n * sizeof(int));
		for(i = 0; i < Offset*n/4; i++) mass[i] = i+1; mass[i] = 0;
		for(i = 0; i < n-1; i++) {
			for(j = 0; j < Blocksize/n; j++) {
				mass[i * Offset + j] = (i+1)*Offset + j;
			}
		}
		for(j = 0; j < Blocksize/n - 1; j++) {
			mass[(n-1) * Offset + j] = j + 1;
		}	
		mass[(n-1)*Offset + j] = 0;
		k = 0;
		rdtscStart = rdtsc();
		for(i = 0; i < Blocksize; i++) {
			k = mass[k];
		}
		rdtscStart = rdtsc() - rdtscStart;
		fprintf(fp1, "%f\n", (float)rdtscStart/Blocksize);
		fprintf(fp2, "%u\n", n);
		free(mass);
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}
