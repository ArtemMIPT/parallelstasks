#include <omp.h>
#define SIZE 64

template<class T> class blockmatrix {
public:
   blockmatrix(size_t lines, size_t columns): lines(lines), columns(columns) { // subject to change
      // Instead of creating full matrix, I am going to create blocks. These blocks in total will be the full matrix. 
      number_of_blocks_in_line = 1 + (columns / SIZE);
      number_of_blocks_in_column = 1 + (lines / SIZE);
      total_number = number_of_blocks_in_column * number_of_blocks_in_line;

      //Like in initial implementation, but now body is an array of pointers on blocks. 
      body = new T*[total_number]; 
      for(size_t i = 0; i < total_number; i++) {
         body[i] = new T[SIZE * SIZE];
         for(size_t j = 0; j < SIZE * SIZE; j++) {
            body[i][j] = 0;
         }
      }
   }

   ~blockmatrix() { // subhect to change
      for(size_t i = 0; i < total_number; i++) {
         delete [] body[i];
      }
      delete [] body;
   }

   T *operator()(size_t line, size_t col) { // *acc(0,0) = 2.34; *acc(i,j) = *acc(i,j+1);
      return block_ptr(line, col);
   }

   // subject to change
   bool mul(blockmatrix const &left, blockmatrix const &right) {
      if (left.columns != right.lines) return false;
      if (lines != left.lines) return false;
      if (columns != right.columns) return false;
      omp_set_num_threads(4);
      #pragma omp parallel shared(left, right) 
      {
         // Three cycles like in matrix multiplication, but here I select blocks in such order. 
         for(size_t right_lines = 0; right_lines < right.lines; right_lines += SIZE) { 
            for(size_t right_col = 0; right_col < right.columns; right_col += SIZE) { 
               #pragma omp for nowait         
               for(size_t left_lines = 0; left_lines < left.lines; left_lines += SIZE) {

                  size_t step_in_right_body = (right_lines / SIZE) * right.number_of_blocks_in_line + right_col / SIZE;
                  size_t step_in_left_body = (left_lines / SIZE) * left.number_of_blocks_in_line + right_lines / SIZE;

                  T *right_block = right.body[step_in_right_body];
                  T *left_block = left.body[step_in_left_body];
                  // Each block - is a mini-matrix, here is a matrix multiplication.   
                  for (size_t i = 0; i < return_size(left_lines, left.lines); i++) {
                     for (size_t j = 0; j < return_size(right_col, right.columns); j++) {
                        size_t sum = 0;
                        for (size_t k = 0; k < return_size(right_lines, right.lines); k++) {
                           sum += left_block[k + i * SIZE] * right_block[j + k * SIZE];
                        }
                        *(block_ptr(i + left_lines, j + right_col)) += sum;
                     }
                  }
               }
            }
         }
      }
      return true;
   }
  
   bool add(blockmatrix const &left, blockmatrix const &right); 
   static void tune(size_t arg) { // subject for change
   }

private:
   size_t number_of_blocks_in_line, number_of_blocks_in_column, total_number;
   size_t lines, columns;
   T **body;

   T *block_ptr(size_t line, size_t col) {
      size_t position_of_block = ((line / SIZE) * number_of_blocks_in_line) + (col / SIZE);
      T *block = body[position_of_block];
      size_t step = SIZE * (line % SIZE) + (col % SIZE);
      return block + step;
   }

   size_t return_size(size_t a, size_t b) {
      size_t x = a + SIZE;
      if(x < b) {
         return x - a;
      } else {
         return b - a;
      }
   }
};