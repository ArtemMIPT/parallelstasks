// Task1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define _CRT_SECURE_NO_WARNINGS

void print_section_table(PIMAGE_SECTION_HEADER pSectionHeader, PIMAGE_NT_HEADERS pPeHeader) {
	for (int i = 0; i < pPeHeader->FileHeader.NumberOfSections; i++) {
		printf("SECTION %8s \n", pSectionHeader->Name);
		printf("\tVirtSize: %6.8x\t\tVirtAddr: %6.8x\n", pSectionHeader->Misc, pSectionHeader->VirtualAddress);
		printf("\traw data offs: %6.8x\t\traw data size: %6.8x\n", pSectionHeader->PointerToRawData, pSectionHeader->SizeOfRawData);
		printf("\trelocation_offs: %6.8x\trelocations: %6.8x\n", pSectionHeader->PointerToRelocations, pSectionHeader->NumberOfRelocations);
		printf("\tline # offs: %6.8x\t\tline #'s: %6.8x\n", pSectionHeader->PointerToLinenumbers, pSectionHeader->NumberOfLinenumbers);
		printf("\tCharacteristics: %6.8x\n\n", (pSectionHeader++)->Characteristics);
	}
}

void print_export_table(PIMAGE_NT_HEADERS pPeHeader, PVOID pImageBase) {
	printf("\n");
	printf("\n");
	printf("Size of ExportTable: %d \n", pPeHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size);
	return;
}

void print_import_table(PIMAGE_NT_HEADERS pPeHeader, PVOID pImageBase) {
	printf("\n");
	printf("\n");
	printf("Size of ImportTable: %d \n", pPeHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size);

	printf("\n");
	PIMAGE_IMPORT_DESCRIPTOR pImport = (PIMAGE_IMPORT_DESCRIPTOR)((unsigned char *)pImageBase + pPeHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	while (pImport -> Characteristics) {
		printf("\n");
		printf("DLL name: %s \n", pImport -> Name + (unsigned char *)pImageBase);
		printf("\n");

		PIMAGE_THUNK_DATA pImageThunk = (PIMAGE_THUNK_DATA)(pImport -> OriginalFirstThunk + (unsigned char *)pImageBase);
		if (pImport -> OriginalFirstThunk == NULL) {
			pImageThunk = (PIMAGE_THUNK_DATA)(pImport -> FirstThunk + (unsigned char *)pImageBase);
		}

		while (pImageThunk -> u1.AddressOfData)
		{
			if (pImageThunk -> u1.Ordinal & IMAGE_ORDINAL_FLAG32)
				printf("func ordinal: %lld\n", pImageThunk -> u1.Ordinal & 0xFFFF);
			else
			{
				printf("\t func hint %d, name: %s \n",
					((PIMAGE_IMPORT_BY_NAME)(pImageThunk -> u1.AddressOfData + (unsigned char *)pImageBase)) -> Hint,
					((PIMAGE_IMPORT_BY_NAME)(pImageThunk -> u1.AddressOfData + (unsigned char *)pImageBase)) -> Name);
			}

			printf("\n");

			pImageThunk++;
		}
		pImport++;
	}
	return;
}

int main(int argc, char* argv[]) {
	wchar_t wtext[1000];
	mbstowcs(wtext, argv[0], strlen(argv[0]) + 1);
	LPWSTR ptr = wtext;
	HANDLE hFile = CreateFile(ptr, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (INVALID_HANDLE_VALUE == hFile) {
		printf("Failed to open %s with error %d \n", argv[0], GetLastError());
		return 1;
	}
	HANDLE hFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY | SEC_IMAGE, 0, 0, NULL);
	if (NULL == hFileMapping) {
		printf("Failed to create mapping %s with error %d \n", argv[0], GetLastError());
		return 2;
	}
	PVOID pImageBase = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
	if (NULL == pImageBase) {
		printf("Failed to map %s with error %d \n", argv[0], GetLastError());
		return 3;
	}

	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)pImageBase;
	printf("DOS HEADER = %c%c 0x%x \n", pDosHeader->e_magic & 0xFF, (pDosHeader->e_magic >> 8) & 0xFF, pDosHeader->e_lfanew);
	PIMAGE_NT_HEADERS pPeHeader = (PIMAGE_NT_HEADERS)(((unsigned char*)pImageBase) + pDosHeader->e_lfanew);
	printf("PE HEADER = #%c%c%x%x# 0x%x sizeof=%s \n",
		pPeHeader->Signature & 0xFF,
		(pPeHeader->Signature >> 8) & 0xFF,
		(pPeHeader->Signature >> 16) & 0xFF,
		(pPeHeader->Signature >> 24) & 0xFF,
		pPeHeader->FileHeader.Machine,
		pPeHeader->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER) ? "OK" : "BAD");
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)(((unsigned char*)pPeHeader) + 4 + sizeof(IMAGE_FILE_HEADER) + pPeHeader->FileHeader.SizeOfOptionalHeader);
	//print_section_table(pSectionHeader, pPeHeader); // Zero task

	print_import_table(pPeHeader, pImageBase); //first task (Import table/export table).
	print_export_table(pPeHeader, pImageBase);

	int n;
	scanf("%d", &n);
	return 0;
}


